/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author lenovo
 */
public class Triangle {
    private double h,b;
    public Triangle(double h,double b){
        this.h = h;
        this.b = b;
    }
    public double calArea(){
        return 0.5 * b * h;
    }
    public double getB(){
         return b;
    }
    public double getH(){
        return h;
    }
    public void setBH(double b,double h){
        if(b<=0){
            System.out.println("Error: Base must more zero!!!");
        }
        this.b = b;
        if(h<=0){
            System.out.println("Error: Hight must more zero!!!");
        }
        this.h = h;
    }
}
