/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author lenovo
 */
public class TestRectangle {
    public static void main(String[] args) {
        Rectangle rectangle1 = new Rectangle(2,4);
        System.out.println("Area of Rectangle ( h = "+ rectangle1.getH() +") ( w = "+rectangle1.getW()+") is "+rectangle1.calArea());
        rectangle1.setHW(3, 9);
        System.out.println("Area of Rectangle ( h = "+ rectangle1.getH() +") ( w = "+rectangle1.getW()+") is "+rectangle1.calArea());
        rectangle1.setHW(0, 0);
        System.out.println("Area of Rectangle ( h = "+ rectangle1.getH() +") ( w = "+rectangle1.getW()+") is "+rectangle1.calArea());
    }
}
