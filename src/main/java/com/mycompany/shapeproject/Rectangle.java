/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.shapeproject;

/**
 *
 * @author lenovo
 */
public class Rectangle {
    private double w,h;
    public Rectangle(double w,double h){
        this.h = h;
        this.w = w;
    }
    public double calArea(){
        return h*w;
    }
    public double getH(){
        return h;
    }
    public double getW(){
        return w;
    }
    public void setHW(double h,double w){
        if(h<=0){
            System.out.println("Error: Hight must more zero!!!");
        }
        this.h = h;
        if(w<=0){
            System.out.println("Error: Width must more zero!!!");
        }
        this.w = w;
    }
}
